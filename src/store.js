import Vuex from 'vuex';
import Vue from 'vue';
import { routes , default as router } from './router';
import config from './config';

// 引入 sync, 用于 router 切换时, 数据存储到 vuex 的 store 中
import { sync } from 'vuex-router-sync'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    routes,
    config
  },
  mutations: {
  }
});

sync(store, router);

export default store;
