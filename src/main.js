import Vue from 'vue';
import App from './App.vue';
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import 'font-awesome/css/font-awesome.css';

import router from './router';
import store from './store';

Vue.use(ViewUI);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
