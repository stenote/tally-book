import Dashboard from '@/views/Dashboard.vue';

// Billing
import Billing from '@/views/Billing/Index.vue';
// 账号 (钱包)
import BillingAccount from '@/views/Billing/Account.vue';
// 明细
import BillingTransaction from '@/views/Billing/Transaction.vue';

// 分类
import Classification from '@/views/Classification/Index.vue';
// 支出
import ClassificationExpense from '@/views/Classification/Expense.vue';
// 收入
import ClassificationIncome from '@/views/Classification/Income.vue';

// 债务
import Debt from '@/views/Debt/Index.vue';

export default [
  {
    path: '/',
    component: Dashboard,
    name: 'dashboard',
    title: '概况',
    icon: 'ios-speedometer-outline',
    breadcrumbs: [
      {
        path: '/',
        title: '概况'
      }
    ],
  },
  {
    path: '/debt',
    component: Debt,
    name: 'debt',
    title: '债务',
    icon: 'ios-flame',
    breadcrumbs: [
      {
        path: '/',
        title: '债务'
      }
    ],
  },
  {
    path: '/billing',
    component: Billing,
    name: 'billing',
    title: '财务',
    icon: 'logo-usd',
    children: [
      {
        path: 'account',
        component: BillingAccount,
        name: 'billing/account',
        title: '账号 (钱包)',
        breadcrumbs: [
          {
            title: '财务',
          },
          {
            path: '/billing/account',
            title: '账号 (钱包)'
          }
        ]
      },
      {
        path: 'transaction',
        component: BillingTransaction,
        name: 'billing/transaction',
        title: '明细',
        breadcrumbs: [
          {
            title: '财务',
          },
          {
            path: '/billing/transaction',
            title: '明细'
          }
        ]
      },
      {
        path: 'classificationExpense',
        component: ClassificationExpense,
        name: 'billing/classification',
        title: '收支分类 (支出)',
        breadcrumbs: [
          {
            title: '财务',
          },
          {
            path: '/billing/classificationExpense',
            title: '收支分类 (支出)'
          }
        ]
      },
      {
        path: 'classificationIncome',
        component: ClassificationIncome,
        name: 'classification/income',
        title: '收支分类 (收入)',
        breadcrumbs: [
          {
            title: '财务',
          },
          {
            path: '/billing/classificationIncome',
            title: '收支分类 (收入)'
          }
        ]
      }
    ]
  }
];
